package com.example.footballmembers;

import com.jayway.restassured.builder.RequestSpecBuilder;
import io.fabric8.kubernetes.api.model.v4_0.Service;
import org.arquillian.cube.kubernetes.annotations.Named;
import org.arquillian.cube.openshift.api.Template;
import org.arquillian.cube.openshift.impl.enricher.AwaitRoute;
import org.arquillian.cube.openshift.impl.enricher.RouteURL;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Objects;
import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

@RunWith(Arquillian.class)
@Template(url = "classpath:it-template.yml")
public class FootballMemberIT {

    @RouteURL("football-members")
    @AwaitRoute
    private URL route;

    @Named("football-members")
    @ArquillianResource
    Service fMembers;

    String name = "Paul Pogba";
    String position = "Midfielder";
    int kitNumber = 6;

    @Test
    public void verifyServiceCreation() {
        assertThat(fMembers).isNotNull();
        assertThat(fMembers.getSpec()).isNotNull();
        assertThat(fMembers.getSpec().getPorts()).isNotNull();
        assertThat(fMembers.getSpec().getPorts()).isNotEmpty();
    }

    @Test
    public void verifySaveAndRetrieve() throws UnsupportedEncodingException{

        RequestSpecBuilder requestSpecBuilder = getRequestSpecBuilder("add");

        given(requestSpecBuilder.build())
                .param("name", URLEncoder.encode(name,"UTF-8"))
                .param("position", position)
                .param("kit", kitNumber)
                .when().post().then().statusCode(200);

        requestSpecBuilder = getRequestSpecBuilder(name);

        given(requestSpecBuilder.build())
                .when().get()
                .then()
                .statusCode(200)
                .body(containsString("Midfielder"));

    }

    private RequestSpecBuilder getRequestSpecBuilder(String path) throws UnsupportedEncodingException {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        String url = "http://%s/footballmembers/";

        if(path != null){
            url = url.concat(URLEncoder.encode(path, "UTF-8"));
        }

        requestSpecBuilder.setBaseUri(String.format(url, Objects.requireNonNull(route).getHost()));

        return requestSpecBuilder;
    }
}
