# How to run this example

## Clone this Repo
Some changes in the [arquillian.xml](src/test/resources/arquillian.xml) are required, so you need to be able to change the source.

### Change the Credentials and Master Address
Even though you use the jenkins service account, the token generated will be different and the master address could also be different. Make the appropriated changes in arquillian.xml](src/test/resources/arquillian.xml)

If you want to change other parameters in the arquillian configuration, you can check the possible values here:
[arquillian-cube-docs](http://arquillian.org/arquillian-cube/#_kubernetes)

## Create these projects
    oc new-project it-pipeline 
    oc new-project it-prod

## Configure the Productio Environment
    oc create -f prod-template.yml -n it-prod
    oc new-app --template=football-members -n it-prod

## Create a Persistent Volume for Jenkins Slave
In this example we are going to use the maven jenkins slave. Since this project uses maven for the app and the test, it is recommended to create a volume to hold those dependencies and make the upcoming builds a lot faster.

In your Jenkins instance, go to <i>Manage Jenkins > Configure System</i>.
In the cloud section, look for a kubernetes pod template called maven.
You will see a dropbox <b>add volume</b>. Select add <b>Persistent Volume Claim</b>.

Map a PVC created by you to the following path: <b>home/jenkins/.m2</b>

![alt text](img/jenkins-pvc.png "Slave PVC")

## Customization required

This test can run using the jenkins slave pod provided service account or configuring the kubernetes client in the [arquillian.xml](src/test/resources/arquillian.xml)

If you prefer to use the service account, remember to add project creation privileges to it.

    oc adm policy add-cluster-role-to-user self-provisioner system:serviceaccount:it-pipeline:jenkins

To promote an image to the production environment, the service account will need permissions to edit imagestreams in it-prod project
    
    oc policy add-role-to-user edit system:serviceaccount:it-pipeline:jenkins -n it-prod


## Create the pipeline in openshift
    oc new-app https://gitlab.com/openshift-samples/arquillian/it-football-members.git --strategy=pipeline

after running this command, your pipeline will be visible and running in the openshift console like this:

![alt text](img/pipeline.png "pipeline view")
